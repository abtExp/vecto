const ndarray = require('./lib/ndarray');
const sum = require('./util/sum');
const product = require('./util/product');
const core = require('./lib/core');

module.exports = {
    ndarray : ndarray,
    sum : sum,
    product : product,
    core : core
}