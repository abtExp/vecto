const form_arr = require('./form_arr');
const calc_shape= require('./calc_shape');
const calc_size = require('./calc_size');
const find_dim = require('./find_dim');
const flatten  = require('./flatten');
const fill = require('./fill');
const form_chunks = require('./form_chunks');
const transpose = require('./transpose');
const arrange  = require('./arrange');


module.exports = {
    form_arr : form_arr,
    calc_shape:calc_shape,
    calc_size : calc_size,
    find_dim : find_dim,
    flatten : flatten,
    fill : fill,
    form_chunks : form_chunks,
    transpose : transpose,
    arrange : arrange
}